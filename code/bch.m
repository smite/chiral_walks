function z = bch(n, palindromic)
% BCH  Returns the nth order terms of the generalized BCH series.
%  z = bch(n, palindromic)
%
%  Returns the nth order terms of z = logm(e^x e^y e^w). If
%  palindromic is true, sets x = w.
%
%  Implements the formula from math-ph/9905012.
%  Uses the symbolic toolbox.

% Ville Bergholm 2014


x = sym('x', [n,1]);
y = sym('y', [n,1]);

F = expm(diag(x, 1))
G = expm(diag(y, 1));
if palindromic
    H = F;
else
    w = sym('w', [n,1]);
    H = expm(diag(w, 1));
end

% logm formula
FGHm1 = F*G*H -eye(n+1)
temp = 1;
z = 0;
for q=1:n
    temp = temp * FGHm1;
    z = z -((-1)^q / q) * temp;
end

z = simplify(z(1, n+1));
