function p_arrival(H, kappa)
% Exciton arrival/capture probability after Rebentrost et al.

% Ville Bergholm 2014

%H = rand_hermitian(d);
d = length(H);
    
if nargin < 2
    % trapping rate
    kappa = 1;
end

% initial state
psi = zeros(d,1);
psi(1) = 1;

rho = vec(psi * psi');

kappa = linspace(0.01, 10, 200);
for j=1:length(kappa)

% trapping Hamiltonian, imaginary, leaches probability from the system
H_trap = zeros(d);
H_trap(end, end) = kappa(j);

% Liouvillian
%L = full(lmul(-1i*H) +rmul(1i*H) +lmul(-H_trap) +rmul(-H_trap));

H_full = H -1i*H_trap;



t = linspace(0, 5, 100);
for k=1:length(t)
    %q = inv_vec(expm(L * t(k)) * rho);
    %prob(k) = real(trace(q));
    %pur(k) = real(trace(q^2)) / prob(k)^2;
    % (always remains pure)

    % equivalently, using just a ket since we have no Lindblad stuff
    w = expm(-1i*H_full*t(k)) * psi;
    temp = norm(w);
    prob(k) = temp^2;
    w = w/temp; % restore norm to 1
    
    % compare to evolution under just H
    r = expm(-1i*H*t(k)) * psi;

    % eig fucks up the order sometimes
    %[v, d] = eig(q);
    %ang(k) = v(:,1)' * w;
    %[d, P] = spectral_decomposition(q, true);
    %ang(k) = sqrt(w' * P{end} * w);

    ang(k) = r' * w;
end

pcapt = 1-prob;
temp = find(pcapt > 0.5, 1, 'first');
if ~isempty(temp)
    thalf(j) = t(temp);
else
    % has not half-arrived yet
    thalf(j) = max(t);
end

end
figure
plot(kappa, thalf)
xlabel('capture rate \kappa')
ylabel('half-arrival time')

if false
figure;
plot(t, pcapt, 'b', t, abs(ang), 'k');
xlabel('t')
legend('Pcapture', 'overlap')
title(sprintf('Capture rate \\kappa = %f', kappa))
end