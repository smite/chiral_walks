function [res, alpha, ttt] = xsim(alpha, ttt, use_rz)
% Simulates a quantum walk in an exciton number preserving circuit.
% (2n+1)-site ring, simple cyclic gate cascade, palindromized.

% Ville Bergholm 2012-2015


global qit

X = qit.sx;
Y = qit.sy;


%% circuit support graph: n-ring
n = 7
dim = 2 * ones(1,n);


%% theta and alpha values to plot
if nargin < 1
    alpha = linspace(0, 2, 29);
end
na = length(alpha);

if nargin < 2
    ttt = linspace(-1, 1, 103);
end
nt = length(ttt);

res = zeros(n, n, na, nt);

if nargin < 3
    use_rz = true;
end


%% indices for the single-exciton subspace
%ind = [17 9 5 3 2]; % 100, 010, 001
ind = fliplr(2.^(0:n-1) + 1);


%% 2q Hamiltonian
% symmetric and antisymmetric exciton number preserving two-qubit Hamiltonians
HS = 0.5*(kron(X, X) +kron(Y, Y)); % symmetric, real
HA = 0.5*(kron(X, Y) -kron(Y, X)); % antisymmetric, imaginary

% should we use (alpha/n)-rotated gate hamiltonians for all the gates, or
% just a single R_z(alpha) and symmetric gates?
if use_rz
    % symmetric case, using R_z to steer
    H = HS;
else
    % rotated case, no R_z
    H = cos(alpha*pi/n)*HS +sin(alpha*pi/n)*HA;
end
gate_dim = {[2 2], [2 2]};
H = lmap(H, gate_dim);


%% gate hamiltonians
qq = {};
for s = 1:n-1
    h = gate.two(H, [s, s+1], dim).data;
    qq{s} = full(h(ind, ind));
end
% close the ring
h = gate.two(H, [n, 1], dim).data; % when H is symmetric, qubit order does not matter
qq{n} = full(h(ind, ind));

% all gates use the same time/angle
t_opt = pi * ones(1, n);



%% circuit
% on which qubit to apply the R_z (does not change the result)
r_target = 2 % 2..n
for j = 1:na
    % Z rotation
    % with our specific circuit all the alphas can be combined into one as
    % far as transfer probability is concerned
    R = R_z(alpha(j) * pi);
    R = gate.single(R, r_target, dim).data;
    R = full(R(ind, ind));

    %gg = qq; % copy
    %if use_rz
    %    %apply R_z by rotating one gate hamiltonian
    %    gg{r_target-1} = R * gg{r_target-1} * R';
    %end
    
    for k = 1:nt
        t = ttt(k) * t_opt;
        % construct the palindromic circuit
        U = eye(n);
        for s = n:-1:1
            temp = expm(-1i * t(s) * qq{s});
            U = temp * U * temp;
            if use_rz && s == r_target
                % apply R_z directly
                U = R' * U * R;
            end
        end
        res(:, :, j, k) = U;
    end
end

% compare to the "simulated hamiltonian"
if 0 && ~use_rz
    hsim = 0;
    for s=1:n
        hsim = hsim +qq{s};
    end
    Usim = expm(-1i * hsim * 2 * ttt * t_opt(1))
    dist = sqrt(n -abs(trace(Usim'*res)))
    return
end

% transfer probabilities
W = abs(res).^2;

if 0
    figure();
    subplot(2,2,1)
    lplot(ttt, 1, W, 2, 1:n);
    subplot(2,2,2)
    lplot(ttt, 2, W, 2, 1:n);
    subplot(2,2,3)
    lplot(ttt, 3, W, 2, 1:n);
    subplot(2,2,4)
    lplot(ttt, 4, W, 2, 1:n);
else
    for from=1:n
        figure();
    for to=1:n
        subplot(3,3,to)
        splot(ttt, alpha, W, from, to);
        p_max(to, from) = max(max(W(to, from,:,:)));
        p_alpha0(to, from) = max(W(to, from, 1,:));
    end
    end
end
p_max
best_transfer_p = max(max(triu(p_max, 1)))
p_alpha0
improvement = p_max-p_alpha0
end


function lplot(t, a, W, from, to)
    plot(t, squeeze(W(to,from,a,:)));
    xlabel('gate time / \pi')
    ylabel('probability')
    title([sprintf('%d', from), ' -> ', sprintf('%d', to),...
           sprintf(', alpha=%d ', a)])
end

function splot(t, a, W, from, to)
    surf(t, a, squeeze(W(to,from,:,:)));
    xlabel('gate time / \pi')
    ylabel('alpha / \pi')
    title(sprintf('%d -> %d', from, to))
end




