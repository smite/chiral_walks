\documentclass[a4paper,10pt]{article}

\usepackage{a4wide}
\usepackage[utf8]{inputenc}  % UTF-8 text encoding: üöä
\usepackage[T1]{fontenc}
\usepackage[english]{babel}  % english language

\usepackage{amsmath,amssymb}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage{graphicx}

\usepackage{color}
\newcommand{\answer}[1]{\textcolor[rgb]{1,0,0}{#1}\\}
\definecolor{gray}{rgb}{0.7,0.7,0.7}
\newcommand{\RED}[1]{\textcolor[rgb]{1,0,0}{#1}}

\begin{document}
\title{Response to referee comments on ``Chiral Quantum Walks''}
\date{\today}

\maketitle



%\setlength{\parindent}{0.7cm}




\noindent  Dear Dr Laura Mazzola,\\

In reply to your letter, \\

\leftskip=1.2cm \rightskip=1cm
%\begin{center}
{\it
%\setlength{\parindent}{1.7cm}
%\hangindent=1.2cm
%\leftskip=1.2cm \rightskip=1cm
Your manuscript entitled "Chiral Quantum Walks" has now been seen by 3
referees, whose comments are appended below. While they find your work of
potential interest, they have raised substantive concerns that in our view
preclude publication of the manuscript in Nature Communications, at least
in its present form.\\}


%\hangindent=1cm
{\it Should further theoretical work allow you to address these criticisms and
make a stronger case on novelty to answer in particular to the concerns of
reviewer \#3, we would be happy to look at a revised manuscript (unless
something similar has been accepted at Nature Communications or appeared
elsewhere in the meantime). However I should stress that, because Nature
Communications strives to provide an efficient editorial service and fast
publication, we are reluctant to see manuscripts undergo multiple rounds of
review and would be unlikely to offer you more than one more chance to
satisfy our reviewers. In the case of eventual publication, the received
date would be that of the revised paper.}\\


%\hangindent=1cm
{\it In the meantime, we hope you will find our referees' comments helpful in
deciding how to proceed. Please do not hesitate to contact us if there is
anything you would like to discuss.}\\

\leftskip=0cm \rightskip=0cm
%\setlength{\parindent}{0.7cm}


\noindent we would like to sincerely thank the referees for the feedback they have provided us
on our manuscript. In response we have taken the time to make
substantial improvements to the paper. This includes adding to and
increasing the focus of the paper on its most novel aspects --- those relating to circuits,
quantum simulation and experiment --- and rewriting technical sections of
the paper so they are at once less formal and easier to
understand.

Below we include our detailed reply to the referees' comments, but
here we summarize our response:

Referee 1 was positive, saying ``I believe the overall results and
ideas are significant, and would recommend publication''. However,
they did have some concerns, namely that the writing was in parts
difficult to follow. Hence we have substantially improved the
technical sections of the paper. Also, the referee asked some
questions about ways in which the circuit we consider here could be
extended, and so we have answered his question here and included some
discussion of this extension in the manuscript.


Referee 2 was uniformly positive, describing our work as
``spectacular'', and so we do not feel the need to reply in detail to
their report. 

Referee 3 did not recommend publication. One aspect they mentioned was
that the paper was overly formal and the writing not always
clear. We feel we have addressed this with the substantial rewriting of
the technical sections, which was also in response to the suggestions
of referee 1. All other negative points made by referee 3 concern
whether or not our summary of the conditions under which a Hamiltonian
leads to time-reversal symmetric evolution is novel. We will address
this point below in our detailed reply, highlighting the changes we
make in response, but we feel that this discussion is only of very
small relevance to the overall novelty of our manuscript. The main
results of our manuscript are regarding extending the ideas of
time-reversal symmetry to circuits, using circuits to simulate
Hamiltonians, proposing a scheme to control time-reversal asymmetry
via simple local gates, and realizing this experimentally. These are
the parts to which referees 1 and 2 refer to in their replies. Referee
3 only refers directly to these aspects once, describing the
experiment as ``nice and stimulating'' but calls it a ``minor part of
the paper''. All the rest of the the comments of referee 3 refer to
our section on Hamiltonian evolutions. We apologise that our
original version gave too much weight to this introductory section.
We have now shortened it so that it only takes up a
column and a half early on in the manuscript, to ensure that the reader is not confused that this is where our main claim to novelty lies.
Our rewrite, made partly thanks to the referee's comments, should make it
clear that the main claim to novelty lies in the later sections. Our
belief is that these later sections alone offer enough novelty for
publication if the referee were to take them into account.

It contains the editors' and referees' comments in black (where response is needed) and our replies are written in \RED{red}.\\


Sincerely,\\
\newline
The authors


\newpage
\section*{Reviewers' comments:}

\subsection*{Reviewer \#1 (Remarks to the Author):}

The paper describes a liquid-state NMR implementation of a prior proposal
by some of the authors (M.F. and J.B.) to use time-reversal asymmetric
Hamiltonians to enhance transport in quantum networks. Since it is
difficult to find physical Hamiltonians that have anti-unitary symmetry,
the authors extend the notion of anti-unitary symmetry to unitary maps (or
circuits), identifying two properties that they call amplitude
time-reversal symmetry and probability time reversal symmetry. Using
coherent control techniques the authors are then able to implement
time-asymmetric gates, even though the underlying physical Hamiltonians are
time-symmetric. This is essentially a quantum simulation scheme.

While I found the presentation a little disjointed, I believe the overall
results and ideas are significant, and would recommend publication in
Nature Communications following revision of the manuscript. My detailed
comments are outlined below.

\begin{itemize}
\item[1.]
The manuscript is difficult to follow in its present form. I found
myself repeatedly returning to the authors' prior (very well-written) paper
(Scientific Reports, 3, 2361, 2013) to get a better understanding of the
key ideas behind what they are trying to do. For example, the clarity of
the presentation would be helped by explicit definitions of what the
authors mean in a contemporary analysis time symmetry and asymmetry.
Readers who are less-familiar with graph/network theory would also benefit
from some explanation of the terminology used. I assume a self-edge is an
on-site node energy term (or a diagonal term of the Hamiltonian in a
node-labeling basis).

\answer{Thanks for raising this point. In response to this, we have made extensive changes in the text to make it more accessible 
for a wide audience. In particular, the technical
parts have been made clearer, as has the organisation of the manuscript. 
Included in this, in relation to graph theoretical 
terminology, we have replaced ``loop'' with the more commonly used 
term ``cycle'' everywhere. Furthermore, we included a definition 
of both this concept and the notion of ``self-edge'' in the text. }

\item[2.]
How would the implementation of the time-reversal asymmetry scale if we
increased the number of qubits in the ring? Suppose we had an 2N+1 spin
ring, would it still be possible to implement a time-reversal asymmetric
sequence using only local phase gates?

\answer{
Yes. The palindromized cyclic gate cascade circuit we use in the experimental section
can be readily extended to 2N+1 spin rings.
The number of two-qubit gates required is twice the number of qubits in the ring.
Furthermore, if one is only interested in probability symmetry, all the phases $\alpha_i$ can be combined into
a single pair of local phase gates, making the 2N+1 qubit case rather analogous to the three-qubit case we
implement experimentally. We think this fully addresses the question the referee was asking. But just to demonstrate the point, we have simulated transfer probability from site 1 to site 3 on a 5-qubit ring. The results are shown in the attached figure.
Again we can see that the transfer probabilities can be substantially enhanced using a non-zero~$\alpha$.
The manuscript has been amended to include these details.
}
{\centering
\includegraphics[width=0.7\linewidth]{5-site-ring}
}


\item[3.]
In the discussion of the magic echo sequence, I believe $\alpha_1 = 0$, $\alpha_2 = \pi$
so the sequence should not be time-reversal asymmetric at any point.

\answer{The referee is right in that the magic echo sequence is not time-reversal
asymmetric at any point. In the discussion we were trying to point out that an analogous
local $\pi$ shift can be applied also to our circuits, leading to a change of sign of the 
simulated Hamiltonian. 
We have now entirely rewritten this part of the discussion. }




\item[4.]
Table I looks awkward, since the right half of the figure is very
sparse. Transposing the table should help this.

\answer{We completely agree with the referee, and have
changed Table I according to his advice.}

\item[5.]
"anlyzing" is mis-spelt in the first paragraph of the second column in Page 5.

\answer{Fixed, thank you.}

\item[6.]
It is not clear what "W.l.o.g. (due to symmetry arguments)" means in the
appendix on Page 7 (column 1).

\answer{We meant ``Without loss of generality'', we have now written out the whole expression.}
\end{itemize}


\subsection*{Reviewer \#2 (Remarks to the Author):}

This is a very interesting and deep paper on the role of time-reversal
symmetry in quantum information: in Hamiltonians and quantum circuits. It
is shown that in fact many of the typical quantum Hamiltonians and circuits
of quantum
information science break the time reversal (anti-unitary) symmetry. The
authors experimentally implement the such gates, demonstrating a chiral
random walk. By controlling local gates they can generate near-perfectly
direct transport.

The idea is new and sound, the implementation standard, but spectacular,
and the paper is very well written. I recommend publications.\\


\answer{We thank the reviewer for the kind report, and for acknowledging the 
novelty of our theoretical and experimental work.}

\subsection*{Reviewer \#3 (Remarks to the Author):}

The paper is devoted to the discussion of chiral quantum walks emerging in
the time-reversal symmetry breaking. Quantum walks are currently
intensively investigated and further theoretical and experimental
investigations are certainly needed. The present paper is devoted to the so
called continuous quantum walks, in essence tight binding models.\\


The authors first give a general classifications of time-asymetric
processes, they then consider a simple circuit three-site circuit that
discuss in details. As mentioned in the conclusions by the authors the
field of transport in the presence of time-reversal breaking has been
widely studied. There were a large number of papers devoted to the study of
tight-binding models in the presence of a piercing magnetic field (see the
seminal paper [27] in the reference list).

It has been very hard for me to extract what is really new in this paper.
It seems to me that many concepts are known, although perhaps put in a more
general formalism here.
I mean for example that the presence of time-reversal breaking leads to
chiral transport, that you need loops (see Table I) in order to have
sensible effects. The whole discussion about direct transport in the 3-site
circuit is, as far as I understand, just related to the presence of states
which are current-currying. Even-odd effects in loops (see again table I)
have been discussed.
It should be considered also that the literature on transport in a
tight-binding model in the presence of an external flux is huge. See for
example in Phys. Rev. B 42, 8282 (1990), J. Phys.: Condens. Matter 9, 2507
(1997) and reference that quote these papers, or the book on transport
including the presence of a magnetic field is (in the continuum) Datta -
Electronic Transport in mesoscopic Systems.

The authors ignore the vast literature on state transfer in spin networks
(see for example the review by Bose arXiv:0802.1224) which treat problems
very closely related to this paper.\\

\answer{
Above, the referee refers to the parts of our manuscript discussing
time-reversal symmetry breaking in Hamiltonian evolutions, in
particular its novelty. As explained in the summary of our response,
we believe this part of our manuscript to be mostly introductory and
not providing a significant portion of the novelty of the manuscript
(it is quite essential in its setting up the remainder of the
manuscript, however). As the referee points out, there is a large
amount of literature on the topic of time-reversal symmetry, arriving
at it from many perspectives and reproduced in many different research
areas. We cannot rule out that our results on tree and bipartite
graphs, and the relation to probability time-symmetry is not contained
somewhere in the literature. However, we do believe our section on
Hamiltonian evolution plays an important role and likely has some
novelty for the following reasons:
(i) Because time-reversal symmetry
is covered in such a range of fields and means different things to
different people, it is important for us to describe in our manuscript
what we believe it means, e.g. we are not interested in
thermodynamical properties like total magnetization but just the
1-exciton subspace.
(ii) We study general graphs, whereas most of the
literature, in particular those in solid-state systems, focus on
simple regular lattices.}


\answer{
In response to the referees comments, we have made our section on
Hamiltonian evolutions shorter to emphasise its primarily introductory
role and not distract attention from the parts of the manuscript with
more significant claims to novelty (time-reversal symmetry in
circuits, simulation, and experiment). We have also cited some of the
suggested references, wishing to make a greater connection to the
past literature.}

I find the experimental part nice and stimulating, it is however a minor
part of the paper..\\

\answer{We are very happy that the referee found the experimental part 
"nice and stimulating". However, we feel that it is not a 
minor part of the paper, it is on equal footing with the theoretical part.  
In order to highlight the experimental part more, we have included further 
details of the experiment in the main text and even changed the title of the paper.}

The paper is written in a way which in some part is, in my opinion,
unnecessarily formal. It is then hard what is new and what is just a
rephrasing of old concepts.\\

\answer{Thanks to the referee for making this point, which is similar
to that made by referee 1. In response to both, we have made
extensive changes to the text to make less formal and more
accessible.}



\end{document}
